import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsersModule } from './users/users.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategies/jwt.strategy';
import { async } from 'rxjs/internal/scheduler/async';
import { ConfigService } from '@nestjs/config';

@Module({
  providers: [AuthService, LocalStrategy, JwtStrategy],
  controllers: [AuthController],
  exports: [AuthService],
  imports: [
    UsersModule, 
    PassportModule, 
    JwtModule.registerAsync({
      useFactory: async (ConfigService: ConfigService) => ({
        secret: ConfigService.get<string>('KEY_SECRET'),
        signOptions: { expiresIn: '2h'}
      }),
      inject: [ConfigService]
    }),
  ]
})
export class AuthModule {}
